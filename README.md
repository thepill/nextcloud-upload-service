
## Install
### Dependencies
```
pip install pyocclient
```

Get `cloud-upload-service.py`


## Usage
### Config
config.yaml
```

url: '<url>'
username: 'scan'
password: '<password>'
interval: 1
uploaded_path: './uploaded'
logfile: '/var/log/scan-upload/upload.log'
watch:
  - path: ./docs/user1
    remote_path:  Scan_Uploads/user1
  - path: ./docs/user2
    remote_path:  Scan_Uploads/user2

```

Run 
```
python cloud-upload-service.py --config config.yaml
```

## Systemd-Service
For running it as a systemd service

```
[Unit]
Description=Scan Upload Service
After=network.target

[Service]
WorkingDirectory=/opt/scan-upload
ExecStart=/usr/bin/python /opt/scan-upload/cloud-upload-service.py  --config /opt/scan-upload/config.yaml

[Install]
WantedBy=multi-user.target
```
