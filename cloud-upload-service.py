import argparse
import owncloud
import datetime
import os
import sys
import time
import ntpath
import yaml

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", dest="config", type=str, default="config.yaml")
args = parser.parse_args()

global config
global oc

def log(message):
    logpath = config['logfile']
    if os.path.getsize(logpath) > 100 * 1024:
        os.remove(logpath)

    timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H%M%S")
    print(timestamp + ' ' + message)
    with open(logpath, "a") as log:
        log.write(timestamp + ' ' + message)

def upload(filePath, remote_path): 
    now = datetime.datetime.now()
    fileName = ntpath.basename(filePath)

    if os.path.isdir(filePath):
        return

    if fileName.startswith("."):
        return

    log('Detected file ' + file + '\n')    

    filesize = 0
    last_fileSize = 0
    filefinished = False

    while not filefinished:
        filesize = os.stat(filePath).st_size
        time.sleep(1)
        last_fileSize = os.stat(filePath).st_size
        if filesize == last_fileSize:
            filefinished = True
        log('Waiting for file ' + filePath + ' to be finished written\n')        


    if not remote_path == '/':
        remote_path = remote_path + '/'

    fileNameWithDate = now.strftime("%Y-%m-%dT%H%M%S") + '_' + fileName
    log('Uploading ' + fileName + ' to ' + config['url'] + ' ' + remote_path +  ' ...\n' )
    
    try:
        oc.login(config['username'], config['password'])    
        oc.put_file(remote_path + '/' + fileNameWithDate, filePath)
    except Exception as e:
        log('Failed to upload ' + fileName + '\n')
        log(str(e) + '\n')
        time.sleep(10)
        return

    log('Uploaded ' + fileName + ' as ' + fileNameWithDate + '\n')

    upath = config['uploaded_path']
    if not os.path.exists(upath):
        os.makedirs(upath)

    os.rename(filePath, upath + '/' + fileNameWithDate)

def config():
    stream = open(args.config, "r")
    c = yaml.load(stream)
    return c

if __name__ == "__main__":
    config = config()
    oc = owncloud.Client(config['url'])

    if not os.path.exists(os.path.dirname(config['logfile'])):
        os.makedirs(os.path.dirname(config['logfile']))

    log('Watching ' + '\n\n' + str(config['watch']) + '\n\n')

    while True:
        for w in config['watch']:
                for file in os.listdir(w['path']):
                    upload(str(w['path']) + '/' + file, w['remote_path'])
                time.sleep(config['interval'])